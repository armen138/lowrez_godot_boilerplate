# lowrez_godot_boilerplate
[![pipeline status](https://gitlab.com/armen138/lowrez_godot_boilerplate/badges/master/pipeline.svg)](https://gitlab.com/armen138/lowrez_godot_boilerplate/-/commits/master)

A boilerplate project and CI setup for the lowrez jam (a game at 64x64 pixel resolution).

## Includes

* Main menu scene
* Font that displays well in low resolutions
* GitLab-CI to automatically build windows/mac/linux/html5
* Jenkinsfile to automatically build windows/mac/linux
* Automatically deploy to gitlab-pages

## Using under CC0 license

* Kenney Mini font (kenney.nl)
* Micro Roguelike (kenney.nl)

## Customization

If you are using this, it is assumed you know your way around godot. That said, a here are a few easy tweaks to customize the menu:

in scripts/landing.gd:

* The text color and selected menu item color are defined near the top. 
* The "action" function (line 23) defines what happens when a menu item is selected

## License

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png)](http://creativecommons.org/publicdomain/zero/1.0/)   
To the extent possible under law, [Armen138](https://gitlab.com/armen138/lowrez_godot_boilerplate) has waived all copyright and related or neighboring rights to lowrez_godot_boilerplate.
