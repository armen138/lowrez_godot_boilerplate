extends Node2D

var text_color = Color(1, 1, 1)
var selected_color = Color(1, 0.89, 0.47)

var is_web = OS.get_name() == "HTML5"
var menu = []
var selected = 0

func _input(event):
	if event.is_action("ui_down") && !event.is_echo() && event.is_pressed():
		select_menu_item(selected + 1)
	if event.is_action("ui_up") && !event.is_echo() && event.is_pressed():
		select_menu_item(selected - 1)
	if event.is_action("ui_select") && !event.is_echo() && event.is_pressed():
		action(menu[selected].name)

func _ready():
	spawn_map()
	spawn_menu()
	select_menu_item(0)

func action(name):
	if name == "quit":
		get_tree().quit()
	if name == "play":
		pass
	if name == "credits":
		pass
	if name == "options":
		pass

func select_menu_item(item):
	menu[selected].set("custom_colors/font_color", text_color)
	selected = clamp(item, 0, menu.size() - 1)
	menu[selected].set("custom_colors/font_color", selected_color)

func spawn_map():
	var map:Node = load("res://scenes/map.tscn").instance()
	get_node("Background").add_child(map)

func spawn_menu():
	var menu_parent:Node = load("res://scenes/menu.tscn").instance()
	get_node("Background").add_child(menu_parent)
	if is_web:
		var quit = menu_parent.get_node("quit")
		menu_parent.remove_child(quit)
		quit.call_deferred("free")
	menu = menu_parent.get_children()
